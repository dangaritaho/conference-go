from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_weather_data(city, state):
    # to get the latitude and longitude
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    # print("this is response:", response.json())
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    # get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()
    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]

        return {
            "temp": temp, "description": desc}
    except (KeyError, IndexError):
        return None



def get_photo(state, city):
    print("state", state)
    print("city", city)
    headers = {
        "Authorization": PEXELS_API_KEY
    }

    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    response = requests.get(url, headers=headers)
    print(response)
    return response.json()["photos"][0]["url"]
    # parameters = {"query": "{city}, {state}", "photos": 1}
    # response = requests.get(url, parameters=parameters, headers=headers)
    # content = json.loads(response.content)
    # try:
    #     return {"location_url": content["photos"][0]["src"]["original"]}

    # except (KeyError, IndexError):
    #     return {"location_url": None}
