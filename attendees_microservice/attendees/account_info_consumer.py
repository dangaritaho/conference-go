from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_account_vo(ch, method, properties, body):
#   content = load the json in body
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.filter(updated__lt=updated).update_or_create(
            email=email,
            defaults={
                'updated': updated,
                'first_name': first_name,
                'last_name': last_name
            }
        )
    else:
        AccountVO.objects.filter(email=email).delete()


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
while True:
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
        # Open a channel to RabbitMQ
        channel = connection.channel()
        #  declare a fanout exchange named "account_info"
        channel.exchange_declare(exchange='account_info', exchange_type='fanout')
        #       declare a randomly-named queue
        result = channel.queue_declare(queue='', exclusive=True)
        #       get the queue name of the randomly-named queue
        update_account = result.method.queue
        #       bind the queue to the "account_info" exchange
        channel.queue_bind(exchange='account_info', queue=update_account)

        print(' [*] Waiting for logs. To exit press CTRL+C')
        channel.basic_consume(queue=update_account, on_message_callback=update_account_vo, auto_ack=True)

        channel.start_consuming()

    except AMQPConnectionError:
        print("It could not connect to RabbitMQ")
