import json
import requests
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import ConferenceVO

def get_conferences():

    url = "http://monolith_service:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )

def poll():
    while True:
        try:
            get_conferences()
        except Exception as e:
            print(e)
        time.sleep(5)


if __name__ == "__main__":
    poll()
