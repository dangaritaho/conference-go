import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()
print("consumer running")
# Set the hostname that we'll connect to
parameters = pika.ConnectionParameters(host='rabbitmq')
# Create a connection to RabbitMQ
connection = pika.BlockingConnection(parameters)
# Open a channel to RabbitMQ
channel = connection.channel()
# Create a queue if it does not exist
channel.queue_declare(queue='presentation_approvals')
channel.queue_declare(queue='presentation_rejections')

while True:
    try:
        def process_approval(ch, method, properties, body):
            print("processing approval")
            data = json.loads(body)
            email = data["email"]
            name = data["presenter"]
            title = data["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conference.go",
                [email],  # why the square brackets
                fail_silently=False,
            )


        def process_rejection(ch, method, properties, body):
            print("processing rejection")
            data = json.loads(body)
            email = data["email"]
            name = data["presenter"]
            title = data["title"]
            send_mail(
                "Your presentation has been rejected",
                f"{name}, we're sad to tell you that your presentation {title} has been rejected",
                "admin@conference.go",
                [email],  # why the square brackets
                fail_silently=False,  # what's the purpose of this?
            )



        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        # Print a status
        print(' [*] Waiting for messages. To exit press CTRL+C')

        channel.start_consuming()

    except Exception as e:
        print(e)
